# Cave Generator

A cellular automata for random generation of caves.

## Building

Requirements:

- Installed CMake version 3.5
- Installed SDL2 library (booth development and runtime)

To build using CMake run:

```bash
mkdir build
cd build
cmake ..
cmake --build .
```

## Controls

- S Key: One smooth iteration
- R Key: Reset cells

## Video

![Demo Video](media/demo.webm)

