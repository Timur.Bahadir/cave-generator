#pragma once

#include "PixelRenderer/PixelRenderer.hpp"

#include <memory>

#include <SDL2/SDL.h>

namespace cg {

class CaveGenerator {
public:
  using size_t = uint16_t;

  enum class TileState : uint8_t {
    Solid,
    Empty,
  };

  CaveGenerator(pr::size_t const ww, pr::size_t const wh, pr::size_t const tw,
                pr::size_t const th, uint8_t const fp);

  void start();

private:
  // Loop
  bool running;

  void app_loop();
  void handle_input();
  void update_pixel_map();

  // Cave generation
  uint8_t const Fill_Percent;
  size_t const Cave_Width;
  size_t const Cave_Height;
  std::unique_ptr<TileState[]> const cave;

  void smooth();
  void reset();

  uint8_t get_neighbour_count(size_t const x, size_t const y);

  // Input
  SDL_Event event;
  bool s_pressed = false, r_pressed = false;

  // Rendering
  pr::WindowSettings const Window_Settings;
  std::unique_ptr<pr::PixelRenderer> const pixel_renderer;
};

} // namespace cg
