#include "CaveGenerator.hpp"

#include <SDL2/SDL.h>

#include "PixelRenderer/SDL_PixelRenderer.hpp"
#include "effolkronium/random.hpp"

#ifdef __WIN32
#include "windows.h"
#endif

#ifdef __WIN32
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance, LPSTR lpCmdLine,
                   int cmdShow)
#else
int main(int argc, char **argv)
#endif // __WIN32
{
  SDL_Init(SDL_INIT_EVERYTHING);

  cg::CaveGenerator cave_generator{900, 900, 100, 100, 45};
  cave_generator.start();

  SDL_Quit();
  return 0;
}

namespace cg {

using Random = effolkronium::random_static;

CaveGenerator::CaveGenerator(pr::size_t const ww, pr::size_t const wh,
                             pr::size_t const tw, pr::size_t const th,
                             uint8_t const fp)
    : Fill_Percent{fp}, Cave_Width{tw}, Cave_Height{th},
      cave{std::make_unique<TileState[]>(Cave_Width * Cave_Height)},
      Window_Settings{"Cave Generator", ww, wh, tw, th},
      pixel_renderer{std::make_unique<pr::SDL_PixelRenderer>(Window_Settings)} {
}

void CaveGenerator::start() {
  reset();
  app_loop();
}

void CaveGenerator::app_loop() {
  while (running) {
    handle_input();
  }
}

void CaveGenerator::handle_input() {
  while (SDL_PollEvent(&event)) {
    switch (event.type) {
    case SDL_QUIT:
      running = false;
      break;
    case SDL_KEYDOWN:
      switch (event.key.keysym.sym) {
      case SDLK_s:
        if (!s_pressed) {
          smooth();

          s_pressed = true;
        }
        break;
      case SDLK_r:
        if (!r_pressed) {
          reset();

          r_pressed = true;
        }
        break;
      }
      break;
    case SDL_KEYUP:
      switch (event.key.keysym.sym) {
      case SDLK_s:
        s_pressed = false;
        break;
      case SDLK_r:
        r_pressed = false;
        break;
      }
      break;
    }
  }
}

void CaveGenerator::update_pixel_map() {
  for (size_t y = 0; y < Cave_Height; ++y) {
    for (size_t x = 0; x < Cave_Width; ++x) {
      switch (cave.get()[y * Cave_Width + x]) {
      case TileState::Solid:
        pixel_renderer->change_pixel(x, y, 0xFFE74C3C);
        break;
      case TileState::Empty:
        pixel_renderer->change_pixel(x, y, 0xFF2C3E50);
        break;
      }
    }
  }

  pixel_renderer->draw();
}

uint8_t CaveGenerator::get_neighbour_count(size_t const x, size_t const y) {
  uint8_t neighbour_count{0};

  if (cave.get()[(y - 1) * Cave_Width + (x - 1)] == TileState::Solid)
    ++neighbour_count;
  if (cave.get()[(y - 1) * Cave_Width + x] == TileState::Solid)
    ++neighbour_count;
  if (cave.get()[(y - 1) * Cave_Width + (x + 1)] == TileState::Solid)
    ++neighbour_count;

  if (cave.get()[y * Cave_Width + (x - 1)] == TileState::Solid)
    ++neighbour_count;
  if (cave.get()[y * Cave_Width + (x + 1)] == TileState::Solid)
    ++neighbour_count;

  if (cave.get()[(y + 1) * Cave_Width + (x - 1)] == TileState::Solid)
    ++neighbour_count;
  if (cave.get()[(y + 1) * Cave_Width + x] == TileState::Solid)
    ++neighbour_count;
  if (cave.get()[(y + 1) * Cave_Width + (x + 1)] == TileState::Solid)
    ++neighbour_count;

  return neighbour_count;
}

void CaveGenerator::smooth() {
  for (size_t y = 1; y < Cave_Height - 1; ++y) {
    for (size_t x = 1; x < Cave_Width - 1; ++x) {
      uint8_t const neighbor_count = get_neighbour_count(x, y);

      if (cave[y * Cave_Width + x] == TileState::Solid && neighbor_count < 4) {
        cave[y * Cave_Width + x] = TileState::Empty;
      } else if (cave[y * Cave_Width + x] == TileState::Empty &&
                 neighbor_count >= 5) {
        cave[y * Cave_Width + x] = TileState::Solid;
      }
    }
  }

  update_pixel_map();
}

void CaveGenerator::reset() {
  srand(time(NULL));
  for (size_t y = 0; y < Cave_Height; ++y) {
    for (size_t x = 0; x < Cave_Width; ++x) {
      if (y == 0 || y == Cave_Height - 1 || x == 0 || x == Cave_Width - 1) {
        cave[y * Cave_Width + x] = TileState::Solid;
      } else {
        auto roll = rand() % 100;

        // uint8_t const roll = Random::get<uint8_t>(0, 100);
        if (roll > Fill_Percent) {
          cave[y * Cave_Width + x] = TileState::Empty;
        } else {
          cave[y * Cave_Width + x] = TileState::Solid;
        }
      }
    }
  }

  update_pixel_map();
}

} // namespace cg
